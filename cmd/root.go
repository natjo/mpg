package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/natjo/mpg/password"
)

var num int
var separator string
var firstCharacter string

var rootCmd = &cobra.Command{
	Use:   "mpg",
	Short: "Generate a memorable password",
	Run: func(cmd *cobra.Command, args []string) {
		generatedPassword, err := password.Generate(num, separator, firstCharacter)
		if err != nil {
			panic(err)
		}
		fmt.Println(generatedPassword)
	},
}

func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

func init() {
	cobra.OnInitialize()
	rootCmd.Flags().IntVarP(&num, "number", "n", 4, "Number of elements of the password")
	rootCmd.Flags().StringVarP(&separator, "separator", "s", "-", "Separator between elements of the password")
	rootCmd.Flags().StringVarP(&firstCharacter, "first-character", "c", "", "First letter of each element")
}
