package password

import (
	"bufio"
	"math/rand"
	"os"
	"time"
)

var FILE_ADJECTIVES = "./static/adjectives.txt"
var FILE_NOUNS = "./static/nouns.txt"

type Data struct {
	adjectives []string
	nouns      []string
}

func newData() (*Data, error) {
	adjectives, err := readFile(FILE_ADJECTIVES)
	if err != nil {
		return nil, err
	}
	nouns, err := readFile(FILE_NOUNS)
	if err != nil {
		return nil, err
	}
	return &Data{adjectives: adjectives, nouns: nouns}, nil

}

func (d *Data) GetRandomAdjective(firstCharacter string) string {
	return getRandomElement(d.adjectives, firstCharacter)
}

func (d *Data) GetRandomNoun(firstCharacter string) string {
	return getRandomElement(d.nouns, firstCharacter)
}

func getRandomElement(data []string, firstCharacter string) string {
	rand.Seed(time.Now().UnixNano())
	r := rand.Intn(len(data))
	element := data[r]
	for firstCharacter != "" && string(element[0]) != firstCharacter {
		r = rand.Intn(len(data))
		element = data[r]
	}
	return element
}

func readFile(path string) (words []string, err error) {
	// Open file
	file, err := os.Open(path)
	if err != nil {
		return words, err
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)

	// Read through file
	for scanner.Scan() {
		words = append(words, scanner.Text())
	}
	return words, nil
}
