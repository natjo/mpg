package password

import (
	"strings"
)

func Generate(pwLen int, separator string, firstCharacter string) (string, error) {
	data, err := newData()
	if err != nil {
		return "", err
	}
	pwSlice := []string{}

	// Append adjectives
	for len(pwSlice) < pwLen-1 {
		pwSlice = append(pwSlice, data.GetRandomAdjective(firstCharacter))
	}

	// Append final noun
	pwSlice = append(pwSlice, data.GetRandomNoun(firstCharacter))

	return strings.Join(pwSlice, separator), nil
}
